package edu.bo.uagrm.ficct.elc105;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author ruddy
 * @project monitorc-socket-server
 * @created 27/04/2022 - 01:39
 */
public class ClientSocket {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 3000);
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            Scanner scanner = new Scanner(System.in);
            String message = null;
            while (!"exit".equalsIgnoreCase(message)) {
                message = scanner.nextLine();
                output.println(message);
                output.flush();
                System.out.println("Server replied: \t" + input.readLine());
            }
            output.close();
            input.close();
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

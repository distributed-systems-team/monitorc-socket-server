package edu.bo.uagrm.ficct.elc105;

import edu.bo.uagrm.ficct.elc105.services.ConnectionListener;
import edu.bo.uagrm.ficct.elc105.services.SocketServer;

/**
 * @author ruddy
 * @project monitorc-socket-server
 * @created 27/04/2022 - 01:39
 */
public class Main {
    public static void main(String[] args) {
        SocketServer socketServer = new SocketServer();
        ConnectionListener connectionListener = socketServer.getConnectionListener();
        connectionListener.addListener(socketServer);
        connectionListener.connect();
    }
}
package edu.bo.uagrm.ficct.elc105.services;

import edu.bo.uagrm.ficct.elc105.utils.Info;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionListener implements Runnable {
    private ServerSocket serverSocket;
    private List<ISocketListener> listeners = new ArrayList<>();

    /**
     * @constuctor
     */
    public ConnectionListener() {
        Info info = Info.getInstance();
        try {
            this.serverSocket = new ServerSocket(Integer.parseInt(info.getData("PORT")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public void addListener(ISocketListener toAdd) {
        listeners.add(toAdd);
    }

    public void connect() {
        // Notify everybody that may be interested.
        for (ISocketListener listener : listeners)
            listener.onConnectedClient();
    }

    @Override
    public void run() {
        try {
            while (true) {
                Socket socketClient = this.serverSocket.accept();
                System.out.println("NEW CLIENT CONNECTED: " + socketClient.getInetAddress().getHostAddress());
                DataReception dataReception = new DataReception(socketClient);
                new Thread(dataReception).start();
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            System.err.println("Error in client socket when accept connection request");
            throw new RuntimeException(e);
        } finally {
            if (this.serverSocket != null) {
                try {
                    this.serverSocket.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}

package edu.bo.uagrm.ficct.elc105.libs;

import edu.bo.uagrm.ficct.elc105.utils.Info;
import org.apache.commons.mail.*;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author ruddy
 * @project monitorc-socket-server
 * @created 28/04/2022 - 09:35
 */
public class Mailer {
    public static void main(String[] args){
        HtmlEmail email = new HtmlEmail();
        Info info = Info.getInstance();
        email.setHostName(info.getData("MAIL_HOST"));
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(info.getData("MAIL_USERNAME"), info.getData("MAIL_PASSWORD")));
        email.setSSLOnConnect(true);
        try {
            email.setFrom(info.getData("MAIL_USERNAME"));
            email.setSubject("TestMail");
            URL url = new URL("http://www.apache.org/images/asf_logo_wide.gif");
            String cid = email.embed(url, "Apache logo");

            // set the html message
            email.setHtmlMsg("<html>The apache logo - <img src=\"cid:"+cid+"\"></html>");

            // set the alternative message
            email.setTextMsg("Your email client does not support HTML messages");
            email.addTo("ruddyq18@gmail.com");
            email.addTo("zuleny.cr@gmail.com");
            email.send();
        } catch (EmailException e) {
            throw new RuntimeException(e);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}

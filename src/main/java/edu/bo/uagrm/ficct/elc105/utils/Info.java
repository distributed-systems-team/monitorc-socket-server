package edu.bo.uagrm.ficct.elc105.utils;

import io.github.cdimascio.dotenv.Dotenv;

/**
 * @author ruddy
 * @project monitorc-socket-server
 * @created 28/04/2022 - 09:56
 */
public class Info {

//    public static final String addressFileENV = "/home/cruz/Escritorio/Electivas/Sistemas Distribuidos/First Project/monitorc-socket-server/src/main/resources/.env";
    public static final String addressFileENV = "/home/ruddy/IdeaProjects/monitorc-socket-server/src/main/resources/.env";
    public Dotenv environmentVariables;
    public static Info info;

    public Info() {
        this.environmentVariables = Dotenv.configure()
                // address file .env
                .directory(this.addressFileENV).load();
    }

    public static Info getInstance() {
        if (info == null) {
            System.out.println("INFO ENVIRONMENT VARIABLES INITIALIZED");
            info = new Info();
        }
        return info;
    }

    public String getData(String description){
        return this.environmentVariables.get(description);
    }
}

package edu.bo.uagrm.ficct.elc105.services;


/**
 * @author ruddy
 * @project monitorc-socket-server
 * @created 27/04/2022 - 01:50
 */
public class SocketServer implements ISocketListener{
    private ConnectionListener connectionListener;

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    /**
     * @constuctor
     */
    public SocketServer() {
        connectionListener = new ConnectionListener();
    }

    @Override
    public void onConnectedClient() {
        new Thread(this.connectionListener).start();
    }
}

package edu.bo.uagrm.ficct.elc105.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author ruddy
 * @project monitorc-socket-server
 * @created 27/04/2022 - 02:06
 */
public class DataReception implements Runnable {

    private Socket socketClient;

    /**
     * @constuctor
     */
    public DataReception(Socket socketClient) {
        this.socketClient = socketClient;
    }

    @Override
    public void run() {
        PrintWriter output = null;
        BufferedReader input = null;
        try {
            output = new PrintWriter(this.socketClient.getOutputStream(), true);
            input = new BufferedReader(new InputStreamReader(this.socketClient.getInputStream()));
            String message = null;
            while ((message = input.readLine()) != null) {
                System.out.println("Sent from the client:\t" + message);
                output.println("RECEIVED: " + message);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
                if (input != null) {
                    input.close();
                    this.socketClient.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
